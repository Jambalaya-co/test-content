---
groups: 
  - Content
  - Settings
  - Other
title:
  group: Settings
  type: input
  label: Title
  value: Two
type:
  group: Settings
  type: input
  label: Type
  value: Type A
option:
  group: Content
  type: select
  label: Option
  value: One
  options:
    - One
    - Two
    - Three
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sem est, vestibulum at rhoncus eu, aliquet non dolor. Morbi sed mi lorem. Nullam cursus augue sapien, vitae malesuada neque sagittis et. 

